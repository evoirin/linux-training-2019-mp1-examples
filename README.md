This is a repository to accompany the Witekio Embedded Linux training guides for the STMicroelectronics STM32MP1 boards.

It belongs to a lab portion outlining the basic steps of creating a character device driver in Embedded Linux. 

There are four branches, one for each chapter.